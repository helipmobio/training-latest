<!DOCTYPE html>
<html>
<head>
	<title>Home page</title>
</head>
<body>
	<?php
		include 'action.php';

		session_start();

        /***
         * Calling the selectData function and fetching the data.
         * @return array
         * @author Heli Patel.
         * @params $tableName
         * @whereConditionArray
         */

		if(isset($_SESSION['username'])){

		$condition1 = array("is_active" =>1);
		
		$rs1=$obj->selectData("registration", $condition1);
	?>

	<table border="1">
	<tr>
		<th>First name:</th>
		<th>Last name:</th>
		<th>Username :</th>
		<th>Gender:</th>
        <th>Edit</th>
        <th>Delete</th>
	</tr>
	
	<?php
	if(mysqli_num_rows($rs1)>0){

    while ($row1 = mysqli_fetch_assoc($rs1)) {

	?>
	<tr>
		<td><?php echo $row1['txt_fn']; ?></td>
		<td><?php echo $row1['txt_ln']; ?></td>
		<td><?php echo $row1['txt_un']; ?></td>
		<td><?php echo $row1['gender']; ?></td>
        <td><a href="editData.php?edit_id=<?php echo $row1['id']; ?>">Edit</a></td>
        <td>
        	<form action="action.php" method="post">
        	<input type="hidden" value="<?php echo $row1['id'];?>" name="delete_id">
        	<input name="delete" type="submit" value="Delete"/>
        	</form>
       	</td>
    </tr>
	
	<?php 		
				}
			} 		

		?>
		<a href="logout.php">Logout</a>
		</table>
		<?php
			
		}
		else{
			echo "please log in";
		
	?>



<h1>Welcome User</h1>
<a href="login.php">Login</a>
<a href="Registration.php">Register</a>

<?php } ?>
</body>
</html>
