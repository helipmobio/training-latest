<?php

	include 'action.php';
	$id = $_GET['edit_id'];
	$condition = array("is_active" =>1,"id" => $id);
    /***
     * Calling the selectData function and fetching the data to edit.
     * @return array
     * @author Heli Patel.
     * @params $tableName
     * @whereConditionArray
     */

	$rs=$obj->selectData("registration", $condition);
	if(mysqli_num_rows($rs)>0){

    while ($row = mysqli_fetch_assoc($rs)) {

?>

<!DOCTYPE html>
<html>
<head>
	<title>Registration page</title>
</head>
<body>
<form action="action.php" method="POST" enctype="multipart/form-data">

<table border="1">
<tr>
<td colspan=2>
<center><font size=4><b>Registration Form</b></font></center>
</td>
</tr>

<tr>
<td>First Name:</td>
<td><input type="text" name="txt_fn" value="<?php echo $row['txt_fn']; ?>"></td>
</tr>

<tr>
<td>Last Name:</td>
<td><input type="text" name="txt_ln" value="<?php echo $row['txt_ln']; ?>"></td>
</tr>

<tr>
<td>Email:</td>
<td><input type="email" name="txt_un" value="<?php echo $row['txt_un']; ?>"></td>
</tr>

<tr>
<td>Gender</td>
<?php $gender =$row['gender']; ?>
<td><input type="radio" name="gender" value="male" size="10" <?php if (isset($gender) && $gender=="male") echo "checked"; ?>>Male
<input type="radio" name="gender" value="Female" size="10" <?php if (isset($gender) && $gender=="Female") echo "checked"; ?>>Female</td>
</tr>

<tr>
	<td>
		password:
	</td>
	<td><input type="password" name="txt_pass" value="<?php echo $row['txt_pass']; ?>">
		<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
	</td>
</tr>

<tr>
<td><input type="reset" value="cancel"></td>
<td><input type="submit" value="Update" name="update"></td>
</tr>

</table>
</form>

<?php 

			}
		}

 ?>
</body>
</html>