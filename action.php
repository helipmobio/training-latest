<?php
require "Database.php";

class action extends Database
{


}

$obj = new action();

/***
 * Records are inserted from here.
 * @return array
 * @author Heli Patel.
 * @params $tableName
 * @wheConditionArray
 */

if (isset($_POST["register"])) {

    try {
        $keyvalueArray = array(
            "txt_fn" => $_POST["txt_fn"],
            "txt_ln" => $_POST["txt_ln"],
            "txt_un" => $_POST["txt_un"],
            "gender" => $_POST["gender"],
            "txt_pass" => $_POST["txt_pass"],
            "is_active" => 1
        );

        if ($obj->insertRecord("registration", $keyvalueArray)) {
            header("location:index.php?msg=Record inserted");
        }

    } catch (Exception $e) {
        echo 'caught Exception:', $e->getMessage();
    }
}
/***
 * Records are updated from here.update_data function is called from Database.php
 * @return array
 * @author Heli Patel.
 * @params $tableName
 * @wheConditionArray
 * @keyValueArray
 */

if (isset($_POST["update"])) {

    try {
        $id = $_POST['id'];
        $condition = array("id" => $id);
        $myArray = array(
            "txt_fn" => $_POST["txt_fn"],
            "txt_ln" => $_POST["txt_ln"],
            "txt_un" => $_POST["txt_un"],
            "gender" => $_POST["gender"],
            "txt_pass" => $_POST["txt_pass"],
        );

        if ($obj->updateData("registration", $condition, $myArray)) {
            header("location:index.php?msg=Record updated");
        }
    }
    catch (Exception $e){
        echo 'caught Exception:',$e->getMessage();
    }
}
/***
 * Records are deleted from here.
 * @return array
 * @author Heli Patel.
 * @prams $tableName
 * @keyValueArray
 * @wheConditionArray
 */

if (isset($_POST['delete'])) {
    try {
        $id = $_POST['delete_id'];
        $condition = array("id" => $id);

        if ($obj->deleteData("registration", $condition)) {
            header("location:index.php?msg=Record deleted");
        }
    }
    catch (Exception $e){
        echo 'Exception caught:',$e->getMessage();
    }
}



	
