<?php
/***
 * @author Heli Patel.
 * @Database class
 */

class Database
{

    public $con;

    /***
     * Database connection.
     * @return array
     * @author Heli Patel.
     * @params host,root,password and database
     */

    public function __construct()
    {

        $this->con = mysqli_connect("localhost", "root", "", "demoapps");

        if (!($this->con)) {
            # code...
            echo "connection failed.";
        }
        return true;
    }

    /***
     * Insert record function.
     * @return array
     * @author Heli Patel.
     * @params $tableName
     * @whereConditionArrray
     */

    public function insertRecord($tableName, $whereConditionArray)
    {

        $sql = "";
        if ($tableName != '' && !empty($whereConditionArray)) {
            $sql .= "INSERT INTO " . $tableName;
            $sql .= " (" . implode(",", array_keys($whereConditionArray)) . ") VALUES ";
            $sql .= "('" . implode("','", array_values($whereConditionArray)) . "')";
            $query = mysqli_query($this->con, $sql);

            if ($query) {
                return true;
            }
            return false;
        }
        return false;

    }

    /***
     * Fetching the data from database.
     * @return array
     * @author Heli Patel.
     * @params $tableName,
     * @whereConditionArray
     */

    public function selectData($tableName, $whereConditionArray, $field =[] )
    {

        $sql = "";
        $condition = "";
        $selectField= " * ";
        if ($tableName != '' && !empty($whereConditionArray)) {

                //$condition = substr($condition, 0, -5);
                //print_r($whereConditionArray);
                $lastElement=end($whereConditionArray);
                $condition .= ' WHERE ';
                foreach ($whereConditionArray as $key => $value) {

                    $condition .= $key . "='" . $value."' ";

                    if( $value != end( $whereConditionArray )){
                        $condition .= " AND ";
                    }
                }

                if(!empty($field)){

                    $selectField = implode(",",($field));
                }


                $sql .= "SELECT ".$selectField." FROM " . $tableName . $condition;
                if ($rs = mysqli_query($this->con, $sql)) {
                    return $rs;
                }
                return false;

        }

        return false;
    }

    /***
     * Updating the data.
     * @return array
     * @author Heli Patel.
     * @params $tableName,
     * @whereConditionarray
     * @keyValueArray
     */

    public function updateData($tableName, $whereConditionArray, $keyValueArray)
    {

        $sql = "";
        $condition = "";
        if ($tableName != '' && !empty($whereConditionArray)) {

            foreach ($whereConditionArray as $key => $value) {

                $condition .= $key . "='" . $value."' ";

                if( $value != end( $whereConditionArray )){
                    $condition .= " AND ";
                }
            }
            //$condition = substr($condition, 0, -5);
            foreach ($keyValueArray as $key => $value) {

                $sql .= $key . "='" . $value."' ";

                if( $value != end( $keyValueArray )){
                    $sql .= ",";
                }
//                $sql .= $key . "='" . $value . "', ";
            }

            //$sql = substr($sql, 0, -2);
            $sql = "UPDATE " . $tableName . " SET " . $sql . " WHERE " . $condition;

            if (mysqli_query($this->con, $sql)) {
                return true;
            }

            return false;
        }

        return false;
    }


    /***
     * Deleting the data.
     * @return array
     * @author Heli Patel.
     * @params $tableName
     * @whereConditionArray
     *
     */

    public function deleteData($tableName, $whereConditionArray)
    {

        $sql = "";
        $condition = "";

        if ($tableName != '' && !empty($whereConditionArray)) {
            foreach ($whereConditionArray as $key => $value) {
                $condition .= $key . "='" . $value."' ";

                if( $value != end( $whereConditionArray )){
                    $condition .= " AND ";
                }
            }
            //$condition = substr($condition, 0, -5);
            $sql .= "DELETE FROM " . $tableName . " WHERE " . $condition;

            if (mysqli_query($this->con, $sql)) {
                return true;
            }
        }
    }


}

?>